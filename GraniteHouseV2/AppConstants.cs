﻿namespace GraniteHouseV2
{
    public class AppConstants
    {
        public static string ImagePath = @"\images\product\";
        public static string SessionCart = "ShoppingCartSession";

        public static string AdminRole = "Admin";
        public static string CustomerRole = "Customer";
    }
}
